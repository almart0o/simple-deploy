
#!/bin/bash
#simpledeploy.sh branch files.txt server path
set -e
if [ "$#" -le 3 ]
then
	echo 'simpledeploy.sh branch files.txt server path'
	exit 1;
fi

BRANCH="$1"
FILES_LIST="$2"
SERVER="$3"
REMOTE_PATH="$4"

read -p "Remote path is '$REMOTE_PATH' Are you sure? It will be purged!" -r  REPLY
echo
case "$REPLY" in 
  y|Y ) echo "starting deploy..";;
  * ) echo "halted" && exit 1 || return 1;;
esac

mkdir -p ./build-sd

git  --work-tree=./build-sd checkout $BRANCH -- .

echo "building project.."
while read -r line
do
cp -r $line ./build-sd/
done < "$FILES_LIST"
cd build-sd
tar -czf build-sd.tar.gz *
echo "done."

echo "uploading build.."
ssh $SERVER "rm -rf $REMOTE_PATH; mkdir $REMOTE_PATH"
scp build-sd.tar.gz $SERVER:"$REMOTE_PATH/"
echo "extracting archive on remote"
ssh $SERVER "cd $REMOTE_PATH; tar -xzf build-sd.tar.gz; rm build-sd.tar.gz; npm i --production"
echo "successfully extracted!"


echo "cleaning up..."
rm -rf build-sd build-sd.tar.gz
echo "Sucessfully builded branch $BRANCH and uploaded on $SERVER to $REMOTE_PATH"

